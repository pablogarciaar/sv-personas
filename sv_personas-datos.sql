--
-- Volcado de datos para la tabla paises
--

INSERT INTO paises (id, nombre) VALUES
(1, 'Afganistán'),
(2, 'Albania'),
(3, 'Alemania'),
(4, 'Andorra'),
(5, 'Angola'),
(6, 'Antigua y Barbuda'),
(7, 'Arabia Saudita'),
(8, 'Argelia'),
(9, 'Argentina'),
(10, 'Armenia'),
(11, 'Australia'),
(12, 'Austria'),
(13, 'Azerbaiyán'),
(14, 'Bangladesh'),
(15, 'Barbados'),
(16, 'Belarús'),
(17, 'Bélgica'),
(18, 'Belice'),
(19, 'Benin'),
(20, 'Bhután'),
(21, 'Bolivia'),
(22, 'Bosnia y Herzegovina'),
(23, 'Botswana'),
(24, 'Brasil'),
(25, 'Brunei Darussalam'),
(26, 'Bulgaria'),
(27, 'Burkina Faso'),
(28, 'Burundi'),
(29, 'Cabo Verde'),
(30, 'Camboya'),
(31, 'Camerún'),
(32, 'Canadá'),
(33, 'Chad'),
(34, 'Chile'),
(35, 'China'),
(36, 'Chipre'),
(37, 'Colombia'),
(38, 'Comoras'),
(39, 'Congo'),
(40, 'Costa Rica'),
(41, 'Costa de Marfil'),
(42, 'Croacia'),
(43, 'Cuba'),
(44, 'Dinamarca'),
(45, 'Djibouti'),
(46, 'Dominica'),
(47, 'Dominicana'),
(48, 'Ecuador'),
(49, 'Egipto'),
(50, 'El Salvador'),
(51, 'Emiratos Árabes Unidos'),
(52, 'Eritrea'),
(53, 'Eslovaquia'),
(54, 'Eslovenia'),
(55, 'España'),
(56, 'Estados Unidos de América'),
(57, 'Estonia'),
(58, 'Etiopía'),
(59, 'Macedonia'),
(60, 'Rusia'),
(61, 'Fiji'),
(62, 'Filipinas'),
(63, 'Finlandia'),
(64, 'Francia'),
(65, 'Gabón'),
(66, 'Gambia'),
(67, 'Georgia'),
(68, 'Ghana'),
(69, 'Granada'),
(70, 'Grecia'),
(71, 'Guatemala'),
(72, 'Guinea'),
(73, 'Guinea Ecuatorial'),
(74, 'Guinea-Bissau'),
(75, 'Guyana'),
(76, 'Haití'),
(77, 'Honduras'),
(78, 'Hungría'),
(79, 'India'),
(80, 'Indonesia'),
(81, 'Irán'),
(82, 'Iraq'),
(83, 'Irlanda'),
(84, 'Islandia'),
(85, 'Islas Marshall'),
(86, 'Islas Salomón'),
(87, 'Israel'),
(88, 'Italia'),
(89, 'Jamahiriya Árabe Libia'),
(90, 'Jamaica'),
(91, 'Japón'),
(92, 'Jordania'),
(93, 'Kazajstán'),
(94, 'Kenya'),
(95, 'Kirguistán'),
(96, 'Kiribati'),
(97, 'Kuwait'),
(98, 'Lesotho'),
(99, 'Letonia'),
(100, 'Líbano'),
(101, 'Liberia'),
(102, 'Liechtenstein'),
(103, 'Lituania'),
(104, 'Luxemburgo'),
(105, 'Madagascar'),
(106, 'Malasia'),
(107, 'Malawi'),
(108, 'Maldivas'),
(109, 'Malí'),
(110, 'Malta'),
(111, 'Marruecos'),
(112, 'Mauricio'),
(113, 'Mauritania'),
(114, 'México'),
(115, 'Micronesia'),
(116, 'Mónaco'),
(117, 'Mongolia'),
(118, 'Montenegro'),
(119, 'Mozambique'),
(120, 'Myanmar'),
(121, 'Namibia'),
(122, 'Nauru'),
(123, 'Nepal'),
(124, 'Nicaragua'),
(125, 'Níger'),
(126, 'Nigeria'),
(127, 'Noruega'),
(128, 'Nueva Zelandia'),
(129, 'Omán'),
(130, 'Países Bajos'),
(131, 'Pakistán'),
(132, 'Palau'),
(133, 'Palestina'),
(134, 'Panamá'),
(135, 'Papua Nueva Guinea'),
(136, 'Paraguay'),
(137, 'Perú'),
(138, 'Polonia'),
(139, 'Portugal'),
(140, 'Qatar'),
(141, 'Reino Unido de Gran Bretaña e Irlanda del Norte'),
(142, 'República Árabe Siria'),
(143, 'República Centroafricana'),
(144, 'República Checa'),
(145, 'República de Corea'),
(146, 'República de Moldova'),
(147, 'República Democrática del Congo'),
(148, 'República Democrática Popular Lao'),
(149, 'República Popular Democrática de Corea'),
(150, 'República Unida de Tanzanía'),
(151, 'Rumania'),
(152, 'Rwanda'),
(153, 'Saint Kitts y Nevis'),
(154, 'Samoa'),
(155, 'San Marino'),
(156, 'San Vicente y las Granadinas'),
(157, 'Santa Lucía'),
(158, 'Santo Tomé y Príncipe'),
(159, 'Senegal'),
(160, 'Serbia'),
(161, 'Seychelles'),
(162, 'Sierra Leona'),
(163, 'Singapur'),
(164, 'Somalia'),
(165, 'Sri Lanka'),
(166, 'Sudáfrica'),
(167, 'Sudán'),
(168, 'Sudán del Sur'),
(169, 'Suecia'),
(170, 'Suiza'),
(171, 'Suriname'),
(172, 'Swazilandia'),
(173, 'Tailandia'),
(174, 'Tayikistán'),
(175, 'Timor-Leste'),
(176, 'Togo'),
(177, 'Tonga'),
(178, 'Trinidad y Tabago'),
(179, 'Túnez'),
(180, 'Turkmenistán'),
(181, 'Turquía'),
(182, 'Tuvalu'),
(183, 'Ucrania'),
(184, 'Uganda'),
(185, 'Uruguay'),
(186, 'Uzbekistán'),
(187, 'Vanuatu'),
(188, 'Vaticano'),
(189, 'Venezuela '),
(190, 'Viet Nam'),
(191, 'Yemen'),
(192, 'Zambia'),
(193, 'Zimbabwe');

--
-- Volcado de datos para la tabla personas
--

INSERT INTO personas (id, id_tipo_documento, num_documento, id_pais, sexo, nombre, apellido, email, telefono, fecha_nacimiento, id_padre) VALUES
( 1, 1, 3620054 , 9, 'M', 'Paule', 'Comford', 'pcomford0@mayoclinic.com', '', '2000-08-03', 0),
( 2, 2, 7275412 , 1, 'M', 'Tammie', 'Stagge', 'tstagge1@opera.com', '', '2000-08-03', 1),
( 3, 2, 3569607 , 1, 'F', 'Clotilda', 'Howe', 'chowe2@google.com.au', '1198985656', '1998-12-10', 1),
( 4, 2, 3154143 , 1, 'F', 'Waring', 'Collisson', 'wcollisson3@wsj.com', '1198985656', '2001-10-10', 2),
( 5, 2, 6908930 , 1, 'F', 'Donelle', 'Keesman', 'dkeesman4@mayoclinic.com', '1198985656', '2000-10-10', 3),
( 6, 2, 7988404 , 1, 'F', 'Abbey', 'Harwell', 'aharwell5@seattletimes.com', '1198985656', '2000-10-10', 0),
( 7, 2, 9814951 , 1, 'F', 'Irma', 'Greenstock', 'igreenstock6@i2i.jp', '1198985656', '2000-10-10', 0),
( 8, 2, 0494365 , 1, 'F', 'Brant', 'Goldstein', 'bgoldstein7@dion.ne.jp', '1198985656', '2000-10-10', 0),
( 9, 2, 3943433 , 1, 'F', 'Nike', 'Gopsill', 'ngopsill8@ox.ac.uk', '1198985656', '2000-10-10', 0),
(10, 1, 8622075 , 1, 'M', 'Juan' , 'Sherewood', 'jsherewood9@npr.org', '1198985656', '2000-10-10', 0),
(11, 1, 0851755 , 1, 'F', 'Falit', 'Ayerst', 'fayersta@myspace.com', '1198985656', '2000-10-10', 0),
(12, 2, 3921859 , 1, 'F', 'Auste', 'Battaille', 'abattailleb@wordpress.com', '1198985656', '2000-10-10', 0),
(13, 2, 6905434 , 5, 'F', 'Sarge', 'Perez', 'mcromptone@fotki.com', '1198985656', '2000-10-10', 0),
(14, 2, 3451842 , 6, 'F', 'Anton', 'Crompton', 'jgamesonf@craigslist.org', '1198985656', '2000-10-10', 0),
(15, 2, 8869708 , 7, 'F', 'Murra', 'Serotsky', 'sridelg@paginegialle.it', '1198985656', '2000-10-10', 70),
(16, 2, 6448187 , 1, 'F', 'Janet', 'Ritmeier', 'jtenwickh@acquirethisname.com', '1198985656', '1998-10-10', 70),
(17, 2, 9121402 , 2, 'F', 'Siobh', 'Tatum', 'sdengeli@blogs.com', '1198985656', '2000-10-10', 73),
(18, 4, 5674238 , 4, 'F', 'Jaymi', 'Falkner', 'conealj@1und1.de', '', '2000-10-10', 0),
(19, 4, 7353420 , 56, 'F', 'Susy', 'Vanessa', 'dserotskyk@macromedia.com', '', '2000-10-10', 1),
(20, 4, 3102038 , 56, 'F', 'Celin', 'Bria', 'abohlensd@utexas.edu', '', '2000-10-10', 0),
(21, 1, 7390034 , 10, 'F', 'Deane', 'Birney', 'sbirneyc@wiley.com', '1198985656', '1998-10-10', 0),
(22, 2, 5283061 , 9, 'F', 'Imoje', 'Bohlens', 'iritmeierl@nbcnews.com', '', '1998-10-10', 0),
(23, 2, 0704551 , 9, 'F', 'Loria', 'Gameson', 'lderricoatm@jigsy.com', '1198985656', '1998-10-10', 0),
(24, 2, 4077433 , 9, 'F', 'Sargey', 'Eldrett', 'vdeaguirren@mtv.com', '', '1998-10-10', 0),
(25, 2, 2558751 , 1, 'F', 'Dean', 'Tarzey', 'Imisonfimisono@usa.gov', '1198985656', '1998-10-10', 0),
(26, 2, 1371347 , 1, 'F', 'Marth', 'Tredwell', 'thockellp@wikia.com', '', '1998-10-10', 0),
(27, 2, 4705382 , 1, 'M', 'Mike', 'Ignazio', 'beldrettq@ihg.com', '1198985656', '1998-10-01', 0),
(28, 4, 8602325 , 1, 'F', 'Don', 'Celestyna', 'itredwellr@sbwire.com', '', '1998-10-10', 0),
(29, 4, 0177900 , 5, 'F', 'Jon', 'Antonina', 'ctarzeys@issuu.com', '1198985656', '1998-10-10', 0),
(30, 4, 4847716 , 6, 'F', 'Jonh', 'Guillet', 'aguillett@artisteer.com', '', '1998-10-10', 0);

--
-- Volcado de datos para la tabla tipos_documento
--

INSERT INTO tipos_documento (id, descripcion) VALUES
(1, 'DNI'),
(2, 'LE'),
(3, 'LC'),
(4, 'PASAPORTE');
