package com.supervielle.challenge.interceptor;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import com.supervielle.challenge.exception.InvalidPersonException;
import com.supervielle.challenge.model.dto.PersonaRequestDTO;
import com.supervielle.challenge.model.dto.PersonaUpdateRequestDTO;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class CustomRequestBodyAdviceAdapter extends RequestBodyAdviceAdapter {

	@Value("${message.invalid.date}")
	private String invalidDate;

	@Value("${message.invalid.age}")
	private String invalidAge;

	@Value("${default.age}")
	private int defaultAge;

	@Value("${date.format}")
	private String dateFormat;

	@Value("${message.invalid.contact}")
	private String invalidContactInfo;

	@Override
	public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
			Class<? extends HttpMessageConverter<?>> converterType) {

		log.info("http request: " + body);

		if (targetType.equals(PersonaRequestDTO.class)) {
			PersonaRequestDTO persona = (PersonaRequestDTO) body;

			// validacion de fecha de nacimiento
			if (StringUtils.isEmpty(persona.getFechaNacimiento())) {
				throw new InvalidPersonException(HttpStatus.BAD_REQUEST.value(), invalidDate);
			} else if (this.menorDeEdad(persona.getFechaNacimiento())) {
				throw new InvalidPersonException(HttpStatus.BAD_REQUEST.value(), invalidAge);
			}

			// validacion de datos de contacto
			if (StringUtils.isEmpty(persona.getEmail()) && StringUtils.isEmpty(persona.getTelefono())) {
				throw new InvalidPersonException(HttpStatus.BAD_REQUEST.value(), invalidContactInfo);
			}
		}
		
		if (targetType.equals(PersonaUpdateRequestDTO.class)) {
			PersonaUpdateRequestDTO pDto = (PersonaUpdateRequestDTO) body;

			// validacion de datos de contacto
			if (StringUtils.isEmpty(pDto.getEmail()) && StringUtils.isEmpty(pDto.getTelefono())) {
				throw new InvalidPersonException(HttpStatus.BAD_REQUEST.value(), invalidContactInfo);
			}
		}

		return super.afterBodyRead(body, inputMessage, parameter, targetType, converterType);
	}

	private boolean menorDeEdad(String fechaNacimiento) {
		LocalDate d = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern(dateFormat));
		if (d.plusYears(defaultAge).isAfter(LocalDate.now()))
			return true;
		return false;
	}

	@Override
	public boolean supports(MethodParameter methodParameter, Type targetType,
			Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}
}