package com.supervielle.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;

import com.supervielle.challenge.config.SwaggerConfig;
import com.supervielle.challenge.model.dto.EstadisticasResponseDTO;
import com.supervielle.challenge.model.dto.PersonaRequestDTO;
import com.supervielle.challenge.model.dto.PersonaResponseDTO;
import com.supervielle.challenge.model.dto.PersonaUpdateRequestDTO;
import com.supervielle.challenge.model.dto.RelacionResponseDTO;
import com.supervielle.challenge.service.EstadisticasService;
import com.supervielle.challenge.service.PersonaService;
import com.supervielle.challenge.service.RelacionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = SwaggerConfig.PERSONAS_CONTROLLER_TAG, value = "PersonasController")
@RestController
@RequestMapping("${api.version}")
public class PersonaController {

	@Autowired
	PersonaService personasService;

	@Autowired
	EstadisticasService statService;

	@Autowired
	RelacionService relacionService;

	/**
	 * Metodo para consulta de personas
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Retorna los datos de la persona por ID", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
			@ApiResponse(code = 500, response = InternalServerError.class, message = "Error en el servidor"),
			@ApiResponse(code = 400, response = BadRequest.class, message = "El servidor no puede procesar la solicitud por un error en el request") })
	@GetMapping(value = "/${personas.path}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PersonaResponseDTO> getPersona(@PathVariable(value = "id") Integer id) {
		PersonaResponseDTO response = personasService.getById(id);
		if (response == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Inserta personas en la base
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Inserta los datos de una nueva persona")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "OK", response = ResponseEntity.class),
			@ApiResponse(code = 500, response = InternalServerError.class, message = "Error en el servidor"),
			@ApiResponse(code = 400, response = BadRequest.class, message = "El servidor no puede procesar la solicitud por un error en el request") })
	@PostMapping("/${personas.path}")
	public ResponseEntity<String> insertPersona(@RequestBody PersonaRequestDTO persona) {
		personasService.insertPerson(persona);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * Actualiza datos de personas en la base
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Actualiza los datos de una persona (email y telefono)")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
			@ApiResponse(code = 500, response = InternalServerError.class, message = "Error en el servidor"),
			@ApiResponse(code = 400, response = BadRequest.class, message = "El servidor no puede procesar la solicitud por un error en el request") })
	@PatchMapping("/${personas.path}")
	public ResponseEntity<String> updatePersona(@RequestBody PersonaUpdateRequestDTO persona) {
		personasService.updatePerson(persona);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Eliminacion de personas de la base
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Elimina los datos de la persona por ID", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
			@ApiResponse(code = 500, response = InternalServerError.class, message = "Error en el servidor"),
			@ApiResponse(code = 400, response = BadRequest.class, message = "El servidor no puede procesar la solicitud por un error en el request") })
	@DeleteMapping(value = "/${personas.path}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PersonaResponseDTO> deletePersona(@PathVariable(value = "id") Integer id) {
		personasService.deletePerson(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Consulta de estadisticas
	 * 
	 * @return
	 */
	@ApiOperation(value = "Retorna los datos estadisticos de cantidad de hombres, cantidad de mujeres y porcentaje de argentinos", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
			@ApiResponse(code = 500, response = InternalServerError.class, message = "Error en el servidor"),
			@ApiResponse(code = 400, response = BadRequest.class, message = "El servidor no puede procesar la solicitud por un error en el request") })
	@GetMapping(value = "/${statistics.path}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EstadisticasResponseDTO> getStatistics() {
		EstadisticasResponseDTO response = statService.getStats();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Consulta de relaciones entre personas
	 * 
	 * @return
	 */
	@ApiOperation(value = "Retorna la relacion entre dos personas (si son hermanos, primos o si el primero es tio del segundo)", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
			@ApiResponse(code = 500, response = InternalServerError.class, message = "Error en el servidor"),
			@ApiResponse(code = 400, response = BadRequest.class, message = "El servidor no puede procesar la solicitud por un error en el request") })
	@GetMapping(value = "/${relaciones.path}/{id1}/{id2}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RelacionResponseDTO> getRelation(@PathVariable(value = "id1") Integer id1,
			@PathVariable(value = "id2") Integer id2) {
		RelacionResponseDTO response = relacionService.getRelacion(id1, id2);
		if (response.getRelacion() != null) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Establece la relacion entre dos personas
	 * 
	 * @param id1
	 * @param id2
	 * @return
	 */
	@ApiOperation(value = "Establece la relacion entre dos personas", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = ResponseEntity.class),
			@ApiResponse(code = 500, response = InternalServerError.class, message = "Error en el servidor"),
			@ApiResponse(code = 400, response = BadRequest.class, message = "El servidor no puede procesar la solicitud por un error en el request") })
	@PostMapping(value = "/${personas.path}/{id1}/padre/{id2}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> setRelation(@PathVariable(value = "id1") Integer id1,
			@PathVariable(value = "id2") Integer id2) {
		if (relacionService.setRelacion(id1, id2)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
