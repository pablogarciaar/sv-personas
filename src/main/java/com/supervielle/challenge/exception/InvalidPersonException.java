package com.supervielle.challenge.exception;

public class InvalidPersonException extends BaseException {
	private static final long serialVersionUID = 4620543925668860586L;

	public InvalidPersonException(int code, String message) {
		super(code, message);
	}
}
