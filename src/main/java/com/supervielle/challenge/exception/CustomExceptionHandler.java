package com.supervielle.challenge.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.supervielle.challenge.model.ApiError;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ DataIntegrityViolationException.class })
	public ResponseEntity<ApiError> handle(DataIntegrityViolationException e, HttpServletRequest request) {
		log.error("Error modificando información la base de datos: {}", e);
		ApiError apiError = ApiError.builder().status(HttpStatus.UNPROCESSABLE_ENTITY.value())
				.message("Infracción de restricción en db, verifique que los datos ingresados sean correctos").build();
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(apiError);
	}

	@ExceptionHandler({ InvalidPersonException.class })
	public ResponseEntity<ApiError> handle(InvalidPersonException e, HttpServletRequest request) {
		ApiError apiError = ApiError.builder().status(e.getStatus()).message(e.getMessage()).build();
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiError);
	}
	
	@ExceptionHandler({ TransactionSystemException.class })
	public ResponseEntity<ApiError> handle(TransactionSystemException e, HttpServletRequest request) {
		ApiError apiError = ApiError.builder().status(HttpStatus.BAD_REQUEST.value()).message(e.getMessage()).build();
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiError);
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<ApiError> handle(Exception e, HttpServletRequest request) {
		log.error("General error: {}", e);
		ApiError apiError = ApiError.builder().status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.message("Error general no contemplado").build();
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(apiError);
	}
}
