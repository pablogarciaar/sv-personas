package com.supervielle.challenge.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 1796490406051843171L;

	protected final int status;
	protected final String message;

}
