package com.supervielle.challenge.repository;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.supervielle.challenge.model.dto.EstadisticasResponseDTO;

@Service
public class EstadisticasRepository {

	private final JdbcTemplate jdbcTemplate;

	final static String STATS_QUERY = "SELECT (sum( if (id_pais=9,1,0)) / count(*) * 100 ) as argentinos, sum( if (sexo='M',1,0)) as hombres,"
			+ " sum( if (sexo='F',1,0)) as mujeres FROM personas";

	@Autowired
	public EstadisticasRepository(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public EstadisticasResponseDTO getData() {
		return jdbcTemplate.queryForObject(STATS_QUERY, null,
				(rs, rowNum) -> new EstadisticasResponseDTO(rs.getInt("mujeres"), rs.getInt("hombres"),
						rs.getDouble("argentinos")));

	}

}
