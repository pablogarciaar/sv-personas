package com.supervielle.challenge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.supervielle.challenge.model.Persona;

@Repository
@Transactional
public interface PersonaRepository extends CrudRepository<Persona, Long> {

	Persona findById(Integer id);

	void deleteById(Integer id);

}
