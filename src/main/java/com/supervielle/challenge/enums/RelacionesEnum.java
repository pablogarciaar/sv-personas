package com.supervielle.challenge.enums;

public enum RelacionesEnum {

	HERMANO("HERMAN@"), PRIMO("PRIM@"), TIO("TI@");

	private final String relacion;

	RelacionesEnum(final String relacion) {
		this.relacion = relacion;
	}

	@Override
	public String toString() {
		return relacion;
	}

}
