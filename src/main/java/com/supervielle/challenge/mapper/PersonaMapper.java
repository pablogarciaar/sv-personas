package com.supervielle.challenge.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.supervielle.challenge.model.Persona;
import com.supervielle.challenge.model.dto.PersonaRequestDTO;
import com.supervielle.challenge.model.dto.PersonaResponseDTO;

@Mapper
public interface PersonaMapper {

	@Mapping(source = "tipoDocumento.descripcion", target = "tipoDocumento")
	@Mapping(source = "pais.nombre", target = "pais")
	PersonaResponseDTO mapToDto(Persona persona);

	@Mapping(source = "tipoDocumento", target = "tipoDocumento.id")
	@Mapping(source = "pais", target = "pais.id")
	@Mapping(target = "id", ignore = true)
	@Mapping(source = "fechaNacimiento", target = "fechaNacimiento", dateFormat = "dd/MM/yyyy")
	@Mapping(target = "padre", constant = "0" )
	Persona mapToEntity(PersonaRequestDTO persona);

}
