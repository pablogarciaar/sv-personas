package com.supervielle.challenge.service;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.supervielle.challenge.mapper.PersonaMapper;
import com.supervielle.challenge.model.Persona;
import com.supervielle.challenge.model.dto.PersonaRequestDTO;
import com.supervielle.challenge.model.dto.PersonaResponseDTO;
import com.supervielle.challenge.model.dto.PersonaUpdateRequestDTO;
import com.supervielle.challenge.repository.PersonaRepository;

@Service
public class PersonaService {

	@Autowired
	private PersonaRepository repository;

	@Autowired
	private EstadisticasService statService;

	private PersonaMapper mapper = Mappers.getMapper(PersonaMapper.class);

	public PersonaResponseDTO getById(Integer id) {
		Persona persona = repository.findById(id);
		PersonaResponseDTO pDto = mapper.mapToDto(persona);
		return pDto;
	}

	public void insertPerson(PersonaRequestDTO personaDto) {
		Persona persona = mapper.mapToEntity(personaDto);
		repository.save(persona);
		statService.clearCache();
	}

	public void deletePerson(Integer id) {
		repository.deleteById(id);
		statService.clearCache();
	}

	public void updatePerson(PersonaUpdateRequestDTO pDto) {

		Persona persona = repository.findById(pDto.getId());

		if (!Strings.isNullOrEmpty(pDto.getEmail())) {
			persona.setEmail(pDto.getEmail());
		}
		if (!Strings.isNullOrEmpty(pDto.getTelefono())) {
			persona.setTelefono(pDto.getTelefono());
		}
		repository.save(persona);
		statService.clearCache();
	}

}
