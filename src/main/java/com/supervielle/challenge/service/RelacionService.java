package com.supervielle.challenge.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.supervielle.challenge.enums.RelacionesEnum;
import com.supervielle.challenge.model.Persona;
import com.supervielle.challenge.model.dto.RelacionResponseDTO;
import com.supervielle.challenge.repository.PersonaRepository;

@Service
public class RelacionService {

	@Autowired
	private PersonaRepository repository;
	
	public RelacionResponseDTO getRelacion(Integer id1, Integer id2) {
		RelacionesEnum relacion = null;
		Persona persona1 = repository.findById(id1);
		Persona persona2 = repository.findById(id2);
		int idPadrePersona1 = persona1 != null ? persona1.getPadre().intValue() : 0;
		int idPadrePersona2 = persona2 != null ? persona2.getPadre().intValue() : 0;
		
		if(idPadrePersona2 != 0 && idPadrePersona1 != 0) {
			if(idPadrePersona1 == idPadrePersona2) {
				relacion = RelacionesEnum.HERMANO;
			} else {
				Persona padrePersona2 = repository.findById(idPadrePersona2);
				int idAbueloPersona2 = padrePersona2 != null ? padrePersona2.getPadre().intValue() : 0;
				if (idAbueloPersona2 != 0) {
					if (idAbueloPersona2 == idPadrePersona1) {
						relacion = RelacionesEnum.TIO;
					} else {
						Persona padrePersona1 = repository.findById(idPadrePersona1);
						int idAbueloPersona1 = padrePersona1 != null ? padrePersona1.getPadre().intValue() : 0;
						if (idAbueloPersona1 != 0) {
							if (idAbueloPersona2 == idAbueloPersona1) {
								relacion = RelacionesEnum.PRIMO;
							}
						}
					}
				}
			}
		}
		
		return RelacionResponseDTO.builder().relacion(relacion != null ? relacion.toString() : null).build();
	}

	public boolean setRelacion(Integer id1, Integer id2) {
		Persona persona = repository.findById(id2);
		if (persona != null) {
			persona.setPadre(id1);
			repository.save(persona);
			return true;
		}
		return false;
	}

}
