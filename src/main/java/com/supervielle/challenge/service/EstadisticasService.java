package com.supervielle.challenge.service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.supervielle.challenge.model.dto.EstadisticasResponseDTO;
import com.supervielle.challenge.repository.EstadisticasRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EstadisticasService {

	private static final String CACHE_ID = "ID";

	LoadingCache<String, EstadisticasResponseDTO> statisticCache = CacheBuilder.newBuilder().maximumSize(10)
			.expireAfterAccess(24, TimeUnit.HOURS).build(new CacheLoader<String, EstadisticasResponseDTO>() {

				@Override
				public EstadisticasResponseDTO load(String id) throws Exception {
					return getFromDB(id);
				}
			});

	@Autowired
	private EstadisticasRepository repository;

	public void clearCache() {
		statisticCache.invalidate(CACHE_ID);
	}

	private EstadisticasResponseDTO getFromDB(String id) {
		EstadisticasResponseDTO data = repository.getData();
		log.info("Recuperando datos desde db: " + data.toString());
		return data;
	}

	public EstadisticasResponseDTO getStats() {
		try {
			return statisticCache.get(CACHE_ID);
		} catch (ExecutionException e) {
			log.error(e.getMessage());
		}
		return null;
	}

}
