package com.supervielle.challenge.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import lombok.Data;

@Data
@Entity
@Table(name = "personas", uniqueConstraints = { @UniqueConstraint(columnNames = { "num_documento", "id_tipo_documento",
		"id_pais", "sexo" }, name = "id_unique_fields") })
@Valid
public class Persona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_documento")
	private TipoDocumento tipoDocumento;

	@Column(nullable = false, name = "num_documento", length = 11)
	private Integer numDocumento;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_pais")
	private Pais pais;

	@Column(nullable = false, name = "sexo")
	private String sexo;

	@Column(nullable = false, name = "nombre")
	private String nombre;

	@Column(nullable = false, name = "apellido")
	private String apellido;

	@Email(message = "Email debe ser una direccion de email valida")
	@Column(name = "email")
	private String email;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "fecha_nacimiento")
	private String fechaNacimiento;

	@Column(name = "id_padre")
	private Integer padre;
}
