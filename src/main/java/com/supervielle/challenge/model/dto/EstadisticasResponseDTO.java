package com.supervielle.challenge.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstadisticasResponseDTO {

	@ApiModelProperty(value = "Cantidad de Mujeres")
	@JsonProperty(value = "cantidad_mujeres")
	private Integer mujeres;

	@ApiModelProperty(value = "Cantidad de Hombres")
	@JsonProperty(value = "cantidad_hombres")
	private Integer hombres;

	@ApiModelProperty(value = "Porcentaje de argentinos")
	@JsonProperty(value = "porcentaje_argentinos")
	private Double argentinos;

}
