package com.supervielle.challenge.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PersonaRequestDTO {

	@ApiModelProperty(value = "Nombre")
	private String nombre;

	@ApiModelProperty(value = "Apellido")
	private String apellido;

	@ApiModelProperty(value = "Sexo")
	private String sexo;

	@ApiModelProperty(value = "Pais")
	private Integer pais;

	@ApiModelProperty(value = "Tipo de documento")
	@JsonProperty(value = "tipo_documento", required = true)
	private Integer tipoDocumento;

	@ApiModelProperty(value = "Numero de documento")
	@JsonProperty(value = "num_documento", required = true)
	private Integer numDocumento;

	@ApiModelProperty(value = "Email")
	private String email;

	@ApiModelProperty(value = "Telefono")
	private String telefono;

	@ApiModelProperty(value = "Fecha de nacimiento")
	@JsonProperty(value = "fecha_nacimiento", required = true)
	private String fechaNacimiento;
}
