package com.supervielle.challenge.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RelacionResponseDTO {

	@ApiModelProperty(value = "Relacion")
	@JsonProperty(value = "relacion")
	private String relacion;

}
