package com.supervielle.challenge.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PersonaUpdateRequestDTO {

	@ApiModelProperty(value = "ID")
	private Integer id;

	@ApiModelProperty(value = "Email")
	private String email;

	@ApiModelProperty(value = "Telefono")
	private String telefono;

}
