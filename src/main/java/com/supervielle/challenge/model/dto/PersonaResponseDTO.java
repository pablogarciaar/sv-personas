package com.supervielle.challenge.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PersonaResponseDTO {

	@ApiModelProperty(value = "ID de persona")
	private Integer id;

	@ApiModelProperty(value = "Nombre de la persona")
	private String nombre;

	@ApiModelProperty(value = "Apellido de la persona")
	private String apellido;

	@ApiModelProperty(value = "Sexo de la persona")
	private String sexo;

	@ApiModelProperty(value = "Nacionalidad")
	private String pais;

	@ApiModelProperty(value = "Tipo de documento de la persona")
	@JsonProperty(value = "tipo_documento")
	private String tipoDocumento;

	@ApiModelProperty(value = "Numero de documento de la persona")
	@JsonProperty(value = "num_documento")
	private Integer numDocumento;

	@ApiModelProperty(value = "E-mail de la persona")
	private String email;

	@ApiModelProperty(value = "Telefono de la persona")
	private String telefono;

	@ApiModelProperty(value = "Fecha de nacimiento de la persona")
	@JsonProperty(value = "fecha_nacimiento")
	private String fechaNacimiento;

}
