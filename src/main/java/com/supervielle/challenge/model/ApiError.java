package com.supervielle.challenge.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Builder;
import lombok.Data;

@JsonPropertyOrder({ "status", "message" })
@Data
@Builder
public class ApiError {

	@JsonProperty("status")
	private int status;

	@JsonProperty("message")
	private String message;

}
