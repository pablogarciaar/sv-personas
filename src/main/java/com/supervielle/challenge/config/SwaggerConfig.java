package com.supervielle.challenge.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final String PERSONAS_CONTROLLER_TAG = "Personas Controller";
	private static final Predicate<RequestHandler> pkg = RequestHandlerSelectors
			.basePackage("com.supervielle.challenge");

	@Value("${api.version}")
	String apiVersion;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).groupName(apiVersion).select().apis(pkg)
				.paths(PathSelectors.ant("/" + apiVersion + "/**")).paths(PathSelectors.any()).build()
				.apiInfo(metaData(apiVersion)).tags(new Tag(PERSONAS_CONTROLLER_TAG,
						"Controlador que expone los endpoints para gestión de personas"));
	}

	private ApiInfo metaData(String version) {
		return new ApiInfoBuilder().title("API PERSONAS").description("API para gestión de personas").version(version)
				.build();
	}

}