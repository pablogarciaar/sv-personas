
# Desafío API Rest - Supervielle -  _Pablo García_
-------------

## Indice

 1. Documentación del proyecto
 2. Configuración de ambiente para API local.
 3. Archivos complementarios.
 4. Link del código fuente.
 5. Endpoint API publicada.
 
-------------

## 1. Documentación

### Objetivo
Aplicación para la gestión del recurso  _Persona_  a través de una API Rest.

### Documentación online de la API en Swagger:
    https://sv-personas.herokuapp.com/swagger-ui.html

### Tecnologías y frameworks utilizados:
* Spring Boot
* Hibernate
* Swagger
* Maven
* Lombok
* Guava Cache
* Map Struct
* MySQL

### Comentarios sobre el enunciado y los criterios tomados:

* API REST con las operaciones CRUD necesarias para gestionar el recurso Persona: Se tuvieron en cuenta las buenas prácticas, por ejemplo:
  * API versionada a través de la URL.
  * Devolución de códigos HTTP estándares por cada tipo de respuesta.
  * Uso de snake_case para JSON
  * Respuestas no encapsuladas.
  * Uso de los métodos correctos donde corresponden (post, get, delete, etc).
  * Ante distintos errores se devuelve una estructura estándar, con código y descripción.
* "_No puede haber personas repetidas_": Para este caso se creó una regla de validación en la base de datos, creando los datos; país, sexo, tipo y número de documento como clave primaria.
* "_Las personas deben tener al menos un dato de contacto_": Para que esto se cumpla se puso un interceptor que valida que el request tenga al menos uno de los dos datos: teléfono o email.
* "_No pueden crearse personas menores a 18 años_": En el mismo interceptor también se valida que la fecha de nacimiento sea una fecha correcta y que sea menor a la fecha actual más 18 años para atrás.
* "_Pueden tener cualquier nacionalidad_": Para esto se creó la tabla países y se vinculó con el campo id_pais de la tabla personas.
* "_Tener en cuenta que la API puede recibir fluctuaciones de tráfico_": Cacheando el dato de estadísticas con Google Guava, se evita que cada consulta de estadísticas acceda a la base de datos, pero para que no quede desactualizado ante modificaciones en la base, en las modificaciónes de la tabla de personas que afectan las estadísticas, los datos de la caché se invalidan forzando así a que la próxima llamada a datos estadísticos acceda nuevamente a la base.
* "_Extender la API para que permita relacionar personas_": Para este punto, por simplicidad se agregó un campo en la tabla personas, id_padre, aunque también podría haberse creado una tabla nueva para relaciones.
Y para ver que relaciones hay se chequea:
  * Son hermanos si ambos tienen el mismo padre.
  * Son primos si ambos tienen el mismo abuelo (el padre de cada uno tiene el mismo padre).
  * ID1 es tío de ID2 si el padre de ID1 es el abuelo de ID2.
  * En cualquier otro caso, se retorna una respuesta vacía con el código HTTP 204 (No Content).

## 2. Configuración para ejecución local:

* Requisitos: Tener instalado java (versión 8 en adelante), Maven (versión 3 en adelante), si se compila desde un IDE, configurar lombok en el [IDE][lombok].
* Disponer de una base de datos MySQL y ejecutar los archivos ``sp-personas-tablas.sql``  y  ``sp-personas-datos.sql`` que están dentro de la carpeta del código fuente, para creación de las tablas y llenado de datos.
* Otra alternativa es cambiar la configuración dentro de ``application.properties``  la propiedad  ``spring.datasource.url`` y utilizar la base de datos de aws para no instalar una base de datos local.
* Dentro del IDE en el proyecto, ejecutar el comando: ``mvn clean install`` y correr la clase ``Application.java`` para levantar el programa.
* Sin el IDE, para generar el jar ejecutable y correrlo desde consola, ejecutar: ``mvn clean package spring-boot:repackage`` el cual generará el archivo ``sv-personas-0.0.1-SNAPSHOT.jar`` dentro de la carpeta _target_.
Ejecutando ``java -jar target/sv-personas-0.0.1-SNAPSHOT.jar`` se ejecutará la aplicación en el puerto 8080.
* Acceder a la documentación: ``http://localhost:8080/swagger-ui.html``
* Desde un administrador de API (Postman o similar) acceder a los distintos endpoints (la colección se adjunta en el código fuente, tanto para API local como Cloud)

## 3. Archivos complementarios 
    
Ubicados dentro del código fuente hay tres archivos:

> sp-personas-tablas.sql → creación de las tablas de la base de datos.
>
> sp-personas-datos.sql → carga de datos para pruebas.
>
> sp-personas.postman_collection.json → Colección de postman para realizar pruebas

## 4. Código fuente 

Disponible en: 
    
    https://bitbucket.org/pablogarciaar/sv-personas/

## 5. Endpoints

URL de la API publicada: 

    https://sv-personas.herokuapp.com/

[//]: # (These are reference links used in the body of this note)

   [lombok]: <https://projectlombok.org/setup/eclipse>