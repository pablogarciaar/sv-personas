--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE paises (
  id int(11) NOT NULL,
  nombre varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE personas (
  id int(11) NOT NULL,
  id_tipo_documento int(11) NOT NULL,
  num_documento int(11) NOT NULL,
  id_pais int(11) NOT NULL,
  sexo varchar(1) NOT NULL,
  nombre varchar(50) NOT NULL,
  apellido varchar(50) NOT NULL,
  email varchar(50) NOT NULL,
  telefono varchar(50) NOT NULL,
  fecha_nacimiento date NOT NULL,
  id_padre int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_documento`
--

CREATE TABLE tipos_documento (
  id int(11) NOT NULL,
  descripcion varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `paises`
--
ALTER TABLE paises
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla `personas`
--
ALTER TABLE personas
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY id_tipo_documento (id_tipo_documento,num_documento,id_pais,sexo),
  ADD UNIQUE KEY id (id);

--
-- Indices de la tabla `tipos_documento`
--
ALTER TABLE tipos_documento
  ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE paises
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE personas
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipos_documento`
--
ALTER TABLE tipos_documento
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
